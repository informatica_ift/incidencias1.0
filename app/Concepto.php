<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    public $table = "concepto";
    
    protected $fillable = [
        'nombre','deparment','id',
     ];
 
     /**
      * The attributes that should be hidden for arrays.
      *
      * @var array
      */
     protected $hidden = [
        'created_at','updated_at',
     ];
}
