<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conexion extends Model
{
    public $table = "conexion";

    protected $fillable = [
        'ipentrante','created_at','updated_at',
     ];
 
     /**
      * The attributes that should be hidden for arrays.
      *
      * @var array
      */
     protected $hidden = [
        'id',
     ];

}
