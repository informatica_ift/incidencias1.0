<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incident;
use App\Setting;
use App\User;
use App\Solution;
use Laracast\Flash\Flash;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\SolveRequest;

class SolucionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('solve');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SolveRequest $request)
    {
        
        if( $request->file('file') != null){
            $solve = new Solution($request->all());
            $file = $request->file('file');
            $namePicture = 'solve_'. time() . '.' . $request->file->extension();
            $path = public_path().'/images/pdf';
                    
            $file->move($path,  $namePicture);
            if ($request->file('file')->isValid()) {
                flash('The picture is not upload: ')->error();
                return redirect()->view('created-solve')->withInput();
            }else{
                $solve->idreciver = $request->idreciver;
                $solve->file = $namePicture;
                $solve->save(); 
            }
        }else{
            $solve = new Solution($request->all());
            $solve->idreciver = $request->idreciver;
            $solve->save();
        }  

        $incident = Incident::where('id',$request->idincident)->first();
        // Send email
        
        try{
            //to how
            $usuarioTO = User::where('id',$incident->iduser) -> first()->toArray();
            
            // from how  
            $usuarioFrom = User::where('id',$incident->idreciver) -> first()->toArray();

            
            Mail::send('emails.solve',['content' => "Incidencia resuelta, ingresar en programa incidencias y ver solucion." , 'trabajador' => $usuarioFrom['name']],function($ms) use ($usuarioFrom, $usuarioTO) {
        
                $ms->from($usuarioFrom['email'] , $usuarioFrom['deparment'].'-'.$usuarioFrom['name']);
        
                $ms->to($usuarioTO['email'])->subject('Incidencia');


            });
        
            //Mail::to($correoTo->email)->queue(new Notificacion($incident));

       }catch(\Exception $e){

            Log::critical($e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage());
            $message = [$e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage()];
            return response()->json($message, 500);
        } 
        //end email


        $incident->delete();

        $usuario = User::where('id',$request->idreciver) -> first();
        $incidents = Incident::where('deparment',$usuario->deparment)->get();
        if($incidents != null){
            flash('Incidencias dada de baja')->error();
            return view('solve')->with('incidents',$incidents)->with('usuario',$usuario);  
        }else{
            flash('No posee incidencias por este momento')->error();
            return view('solve');    
        }

        
        
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list($id)
    {
        $usuario = User::where('id',$id) -> first();
        $incidents = Incident::where('concepto',$usuario->deparment)->get();
        if($incidents != null){
            return view('solve')->with('incidents',$incidents)->with('usuario',$usuario);  
        }else{
            flash('No posee incidencias por este momento')->error();
            return view('solve');    
        }
    }

    public function add(Request $request)

    {
        $incident = Incident::where('id',$request->idincident)->first();
        $incident->idreciver = $request->iduser;
        $incident->status = "revisión";
        $incident->save();
        $usuario = User::where('id',$request->iduser) -> first();
        return view('solves.create-solve')->with('usuario',$usuario)->with('incident',$incident);  
        
    }

}
