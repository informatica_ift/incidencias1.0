<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incident;
use App\Setting;
use App\User;
use App\Solution;

use Laracast\Flash\Flash;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       dd("echo  funciono");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $answer = Solution::where('id',$id) -> first();
        $usuario = User::where('id',$answer->iduser) -> first();
        $usuarioR = User::where('id',$answer->idreciver) -> first();
        return view('answers.edit-answer')->with('answer',$answer)->with('usuario',$usuario)->with('usuarior',$usuarioR);  
          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function list($id)
    {
        $usuario = User::where('id',$id) -> first();
        $solutions = Solution::where('deparment',$usuario->deparment)->get();
        if($solutions != null){
            return view('answers')->with('solutions',$solutions)->with('usuario',$usuario);  
        }else{
            flash('No posee incidencias por este momento')->error();
            return view('answers');    
        }
    }
}
