<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incident;
use App\Setting;
use App\User;
use App\Concepto;
use Laracast\Flash\Flash;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\IncidentsRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\Notificacion;



class InsidenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('incidencias.create-incident') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncidentsRequest $request)
    {
       try{

       
        $incident = new Incident($request->all());
        
        if( $request->file('picture') != null){
           
            $picture = $request->file('picture');
            $namePicture = 'incident_'. time() . '.' . $request->picture->extension();
            $path = public_path().'/images/incidents';
                    
            $picture->move($path,  $namePicture);
            if ($request->file('picture')->isValid()) {
                flash('The picture is not upload: ')->error();
                return redirect()->view('created-incident')->withInput();
            }else{
                $incident->concepto = $request->concepto;
                $incident->photo = $namePicture;
                $incident->save();
            }
        }else{
            $incident->concepto = $request->concepto;
            $incident->photo = "no-imagen.png";
            $incident->save();
        } 

        

            $usuario = User::where('id',$request->iduser) -> first();
        
        // Send email
        
        try{
        
            $usuario = User::where('id',$request->iduser) -> first()->toArray();
            $correoTo = Setting::where('deparment',$incident->concepto)->first()->toArray();
            $reporte = $incident->toArray();
           
            
            Mail::send('emails.mail',['id' => $reporte['id'] ,'content' => $reporte['descript'] , 'trabajador' => $reporte['worker']],function($ms) use ($usuario,$correoTo,$reporte) {
        
                $ms->from($usuario['email'] , $usuario['deparment'].'-'.$usuario['name']);
        
                $ms->to($correoTo['email'])->subject('Incidencia-'.$reporte['title']);


            });
        
            //Mail::to($correoTo->email)->queue(new Notificacion($incident));

       }catch(\Exception $e){

            Log::critical($e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage());
            $message = [$e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage()];
            return response()->json($message, 500);
        } 
        //end email
        $usuario = User::where('id',$request->iduser) -> first();
        $incidents = Incident::where('deparment',$usuario->deparment)->get();
        if($incidents != null){
            flash('Incidencia reportada')->success();
            return view('incident')->with('incidents',$incidents)->with('usuario',$usuario);  
        }else{
            flash('No posee incidencias por este momento')->error();
            return view('incident')->error();    
        }
        }catch(\Exception $e){
            Log::critical($e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage());
            $message = [$e->getCode() . ', ' . $e->getLine() . ', ' . $e->getMessage()];
            return response()->json($message, 500);
        } 

        
            
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $incident = Incident::where('id',$id)->first();
        $usuario = User::where('id',$incident->iduser) -> first();
        return view('incidencias.edit-incident')->with('incident',$incident)->with('usuario',$usuario);   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add($id)

    {
        $usuario = User::where('id',$id) -> first();
        $settings = Setting::all();
        $conceptos = Concepto::all();
        return view('incidencias.create-incident')->with('usuario',$usuario)->with('settings',$settings)->with('conceptos',$conceptos);  
        
    }

    public function list($id)
    {
        $usuario = User::where('id',$id) -> first();
        $incidents = Incident::where('deparment',$usuario->deparment)->get();
        if($incidents != null){
            return view('incident')->with('incidents',$incidents)->with('usuario',$usuario);  
        }else{
            flash('No posee incidencias por este momento')->error();
            return view('incident');    
        }
    }

}

