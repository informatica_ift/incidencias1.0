<?php

namespace App\Http\Controllers;
use App\Concepto;
use App\Setting;

use Illuminate\Http\Request;

class ConceptoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conceptos = Concepto::all();
        return view('concepto')->with('conceptos',$conceptos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $concepto = new Concepto($request->all());
        $concepto->save();
        $conceptos = Concepto::all();
        return view('concepto')->with('conceptos',$conceptos);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $concepto = Concepto::where('id',$id)->first();
        $settings = Setting::all();
        return view('conceptos.edit-concepto')->with('settings',$settings)->with('concepto',$concepto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Concepto::find($id)->update($request->all());
        $conceptos = Concepto::all();
        return view('concepto')->with('conceptos',$conceptos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $concepto = Concepto::where('id',$id)->first();
        $concepto->delete();
        $conceptos = Concepto::all();
        return view('concepto')->with('conceptos',$conceptos);
    }

    public function new()
    {
        $settings = Setting::all();
        return view('conceptos.create-concepto')->with('settings',$settings);

    }
}
