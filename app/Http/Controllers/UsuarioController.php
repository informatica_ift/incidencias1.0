<?php

namespace App\Http\Controllers;
use App\User;
use App\Setting;

use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view('usuario')->with('usuarios',$usuarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $usuario = new User($request->all());
        $usuario->deparment = $request->concepto;
        $usuario->password =  bcrypt($request->$password);
        $usuario->save();
        $usuarios = User::all();
        return view('usuario')->with('usuarios',$usuarios);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $usuario = User::where('id',$id)->first();
        $settings = Setting::all();
        
        return view('usuarios.edit-usuario')->with('usuario',$usuario)->with('settings',$settings);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario = User::where('id',$id)->first();
        if($request->deparment === "null"){
            $deparment = $usuario->deparment;
        }else{
            $deparment = $request->deparment;
        }
        
        if($request->password === null){
            $usuario->name = $request->name;
            $usuario->deparment = $deparment;
            $usuario->email = $request->email;
            $usuario->save();
            $usuarios = User::all();
            return view('usuario')->with('usuarios',$usuarios);
            
        }else{
            $usuario->name = $request->name;
            $usuario->deparment = $deparment;
            $usuario->email = $request->email;
            $usuario->password = bcrypt($request->password);
            $usuario->save();
            $usuarios = User::all();
            return view('usuario')->with('usuarios',$usuarios);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::where('id',$id)->first();
        $usuario->delete();
        $usuarios = User::all();
        return view('usuario')->with('usuarios',$usuarios);
    }

    public function new()
    {
        $settings = Setting::all();
        return view('usuarios.create-usuario')->with('settings',$settings);

    }

}
