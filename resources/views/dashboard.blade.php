@extends('template.main')


@section('title') 
   Escritorio 
@endsection

@section('content') 
<div class="card col-12 ">
  <div class="card-body">
    Departamento: {{$usuario->deparment}}<br/>
    Responsable: {{$usuario->name}}<br/>
    Correo contacto: {{$usuario->email}}<br/>
  </div>
</div>
<div class="card top espacio" style="width: 10rem; height:10rem">
    <a href="{{ url ('incidents/add', $usuario->id) }}">
        <img class="card-img-top" src="{{ asset('images/insidensias.png') }}">
    </a>    
</div>

<div class="card top espacio" style="width: 10rem; height:10rem">
<a href="{{ url ('answers/list', $usuario->id) }}">
    <img class="card-img-top" src="{{ asset('images/soluciones.jpg') }}" alt="reporte insidencia">
</a>  
  
</div>

@endsection