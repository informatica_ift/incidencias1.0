@extends('template.main')


@section('title') 
  Soluciones
@endsection

@section('content') 


<div class="col-12 top-30">
  @include('flash::message')
</div>

<div class="col mayuscula">
<table class="table top-30">
  <thead class="thead-inverse">
    <tr>
      <th>#</th>
      <th>Departamento</th>
      <th>Concepto</th>
      <th>Trabajador</th>
      <th>Dia Publicaci&oacute;n</th>
      <th style="text-align: center;">Opciones</th>
    </tr>
  </thead>
  <tbody>
      @foreach($incidents as $insident)
        <tr>
          <td>{{$insident->id}}</td>
          <td>{{$insident->deparment}}</td>
          <td>{{$insident->title}}</td>
          <td>{{$insident->worker}}</td>
          <td>{{$insident->created_at->format('d/m/Y')}}</td>
          <td style="text-align: center;">
          {!! Form::open(['url' => 'solves/add','method' => 'POST']) !!}
              
              <input type="hidden" value="{{$insident->id}}" name="idincident">
              <input type="hidden" value="{{$usuario->id}}" name="iduser">
              <button type="submit" class="custom"><i class="fas fa-eye"></i></button>
          {!! Form::close()!!}    
          
          </td>
            </div>
          </td>  
        </tr>
      @endforeach
  </tbody>
</table>
</div>
    
@endsection