@extends('template.main')


@section('title') 
  Reportar Incidencia
@endsection

@section('content')

{!! Form::open(['route' => 'incidents.store','method' => 'POST', 'files' => true,'class' => 'col-12']) !!}
<div class="col-12 top-30">
  @include('flash::message')
</div> 
@if(count($errors) > 0)
<div class="alert alert-warning" role="alert">
    <ul>
    @foreach($errors->all() as $error)
        <li><strong>Warning!</strong>{!!$error!!}</li>
    @endforeach 
    </ul>   
</div>
@endif

  <div class="form-group">
    <h1>Reporte de Incidencias</h1>
    <label for="deparment">Deparatamento:</label>
    <select class="custom-select custom-select-lg mb-3" name="concepto" id="idconcepto" onchange="data_concepto($('#idconcepto').val())">
        <option value="null">Seleccionar una opci&oacute;n...</option> 
        @foreach ($settings as $setting)
          <option value="{{$setting->deparment}}">{{$setting->deparment}}</option>
        @endforeach
    </select>
  </div>

  <div class="form-group">
    <label for="concept">Concepto:</label>
    <select class="custom-select custom-select-lg mb-3" name="title" id="idtitle">
        
    </select>
  </div> 

  <div class="form-group">
    
    <label for="descryp">Observaciones:</label>
    <textarea class="form-control" name="descript" rows="3"></textarea>
    <label for="descryp">Firma:</label>
    <input type="text" name="worker" class="form-control">
  </div>
  
  <input type="hidden" value="{{$usuario->id}}" name="iduser">
  <input type="hidden" value="{{$usuario->deparment}}" name="deparment">
  <input type="hidden" value="enviada" name="status">
  
  <div class="mobile">
    <input accept="image/*" type="file" class="upload-button" id="picture" name="picture" capture/>
    <label for="picture">Tomar Foto</label>
    
  </div>  
  
  <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i>&nbsp;Reportar</button>
  <a href="{{ url ('incidents/list', $usuario->id) }}" class="btn btn-primary "><i class="fas fa-undo"></i>&nbsp;Volver</a>  
  {!! Form::close() !!}
  
@endsection