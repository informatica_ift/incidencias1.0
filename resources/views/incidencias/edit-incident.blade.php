@extends('template.main')


@section('title') 
  Ver Incidencia
@endsection

@section('content')
  <div class="form-group col-12">
    <label for="title">Departamento:</label>
    <input type="text" name="deparment" value="{{$incident->deparment}}" disabled class="form-control mayuscula">
    <label for="title">Concepto:</label>
    <input type="text" name="title" value="{{$incident->title}}" disabled class="form-control mayuscula">
    <label for="worker">Firma:</label>
    <input type="text" name="worker" value="{{$incident->worker}}" disabled class="form-control mayuscula">
    <label for="status">Estatus:</label>
    <input type="text" name="status" value="{{$incident->status}}" disabled class="form-control mayuscula">
    <label for="created_at">Fecha:</label>
    <input type="text" name="created_at" value="{{$incident->created_at->format('d/m/Y')}}" disabled class="form-control ">
    <label for="descript">Observacion:</label>
    <textarea name="descript" rows="3" disabled class="form-control">{{$incident->descript}}</textarea>
    @isset($incident->photo)
      @if ($incident->photo <> "no-imagen.png")
        <label class="col-12">Imagen referencia</label>
        <img src="../images/incidents/{{$incident->photo}}" class="col-12">
        <br/>
      @endif  
    @endisset
    <a href="{{ url ('incidents/list', $usuario->id) }}" class="btn btn-primary top"><i class="fas fa-undo"></i>&nbsp;Volver</a>
  </div>
    
@endsection