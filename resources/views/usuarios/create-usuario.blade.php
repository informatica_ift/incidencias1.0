@extends('template.main3')


@section('title') 
  Crear Usuario
@endsection

@section('content')

{!! Form::open(['route' => 'usuarios.store','method' => 'POST', 'files' => false,'class' => 'col-12']) !!}
<div class="col-12 top-30">
  @include('flash::message')
</div> 
@if(count($errors) > 0)
<div class="alert alert-warning" role="alert">
    <ul>
    @foreach($errors->all() as $error)
        <li><strong>Warning!</strong>{!!$error!!}</li>
    @endforeach 
    </ul>   
</div>
@endif

  <div class="form-group">
    <h1>Crear Usuario</h1>
    {{Form::label('name', 'Nombre')}}
    {{Form::text('name', null, ['class' => 'form-control'])}}
    <label for="deparment">Deparatamento:</label>
    <select class="custom-select custom-select-lg mb-3" name="concepto" id="idconcepto">
        <option value="null">Seleccionar una opci&oacute;n...</option> 
        @foreach ($settings as $setting)
          <option value="{{$setting->deparment}}">{{$setting->deparment}}</option>
        @endforeach
    </select>
    {{Form::label('email', 'Correo')}}
    {{Form::text('email', null, ['class' => 'form-control'])}}
    {{Form::label('password', 'Contraseña')}}
    {{Form::password('password',['class' => 'form-control'])}}
    <br/>
    {{Form::submit('Crear',['class' => 'btn btn-primary'])}}
    <a href="{{ url ('dashboardsistem/back') }}" class="btn btn-primary "><i class="fas fa-undo"></i>&nbsp;Volver</a> 

   
  {!! Form::close() !!}
  
@endsection