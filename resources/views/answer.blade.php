@extends('template.main')


@section('title') 
  Incidencia
@endsection

@section('content') 


<div class="col-12 top-30">
  @include('flash::message')
</div>

<div class="col mayuscula">
<table class="table top-30">
  <thead class="thead-inverse">
    <tr>
      <th>#</th>
      <th>Departamento</th>
      <th>Concepto</th>
      <th>Trabajador</th>
      <th>Dia Publicaci&oacute;n</th>
      <th style="text-align: center;">Opciones</th>
    </tr>
  </thead>
  <tbody>
      @foreach($solutions as $solution)
        <tr>
          <td>{{$solution->id}}</td>
          <td>{{$solution->deparment}}</td>
          <td>{{$solution->title}}</td>
          <td>{{$solution->worker}}</td>
          <td>{{$solution->created_at->format('d/m/Y')}}</td>
          <td style="text-align: center;">
              {!! Form::open(['url' => 'answers','method' => 'POST', 'files' => false]) !!}
                <input type="hidden" value="{{$usuario->id}}" name="iduser">
                <input type="hidden" value="{{$solution->id}}" name="idincident">
                <button type="submit" class="btn btn-primary"><i class="fas fa-eye"></i></button>
              {{!! Form::close !!}}
          </td>
            </div>
          </td>  
        </tr>
      @endforeach
  </tbody>
</table>
</div>
    
@endsection