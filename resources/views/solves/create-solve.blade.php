@extends('template.main')


@section('title') 
  Dar Soluci&oacute;n
@endsection

@section('content')
<div class="col-12 top-30">
  @include('flash::message')
</div>
@if(count($errors) > 0)
<div class="alert alert-warning" role="alert">
    <ul>
    @foreach($errors->all() as $error)
        <li><strong>Warning!</strong>{!!$error!!}</li>
    @endforeach 
    </ul>   
</div>
@endif


<div class="form-group col-12">
    {!! Form::open(['route' => 'solves.store','method' => 'POST', 'files' => true]) !!}
    <label for="title">Departamento:</label>
    <input type="text" name="deparment" value="{{$incident->deparment}}" disabled class="form-control mayuscula">
    <label for="title">Concepto:</label>
    <input type="text" name="title" value="{{$incident->title}}" disabled class="form-control mayuscula">
    <label for="worker">Firma:</label>
    <input type="text" name="worker" value="{{$incident->worker}}" disabled class="form-control mayuscula">
    <label for="created_at">Fecha:</label>
    <input type="text" name="created_at" value="{{$incident->created_at->format('d/m/Y')}}" disabled class="form-control ">
    <label for="descript">Observaci&oacute;n:</label>
    <textarea name="descript" rows="3" disabled class="form-control">{{$incident->descript}}</textarea>
    <label for="descript">Soluci&oacute;n:</label>
    <textarea name="solve" rows="3" class="form-control"></textarea>
    <label for="file">Archivo PDF, manual de soluci&oacute;n:</label>
    <input type="file" name="file" class="form-control" id="inputGroupFile01">
    <input type="hidden" value="{{$incident->iduser}}" name="iduser">
    <input type="hidden" value="{{$incident->deparment}}" name="deparment">
    <input type="hidden" value="{{$incident->id}}" name="idincident">
    <input type="hidden" value="{{$incident->title}}" name="title">
    <input type="hidden" value="{{$incident->worker}}" name="worker">
    <input type="hidden" value="{{$incident->descript}}" name="descript">
    <input type="hidden" value="{{$incident->created_at}}" name="date_publi">
    <input type="hidden" value="{{$incident->photo}}" name="photo">
    <input type="hidden" value="{{$incident->idreciver}}" name="idreciver">
    @isset($incident->photo)
      @if ($incident->photo <> "no-imagen.png")
        <label class="col-12">Imagen referencia</label>
        <img src="/../images/incidents/{{$incident->photo}}" class="col-12">
        <br/>
      @endif  
    @endisset
    <br/>
    
  <button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i>&nbsp;Dar Solucionci&oacute;n</button>
  <a href="{{ url ('solves/list', $usuario->id) }}" class="btn btn-primary "><i class="fas fa-undo"></i>&nbsp;Volver</a>  
  {!! Form::close() !!}   
</div>

<br/>
 
@endsection