@extends('template.main2')


@section('title') 
   Bienvenido 
@endsection


@section('content') 

<div class="col-12 top-30">
  @include('flash::message')
</div>
<div class="flex-container2 top">
    <div class="flex-item2">
        {{ Form::open(['url' => 'login/user']) }}
            {!!Form::label('email', 'Correo', array('class' => 'awesome'));!!}
            {!!Form::text('email'); !!}<br/>
            {!!Form::label('psw', 'Contraseña', array('class' => 'awesome'));!!}
            {!!Form::password('psw', null, array('placeholder'=>'Clave')); !!}<br/>  
            {!! Form::submit('Ingresar', ['class' => 'btn btn-outline-warning']) !!}      
        {{ Form::close() }}
         
    </div>
</div>
@endsection
