@extends('template.main')


@section('title') 
  Incidencia
@endsection

@section('content') 
<div class="col-12 bottom">
<a href="{{ url ('incidents/add', $usuario->id) }}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"><i class="fas fa-file-alt"></i>&nbsp;Nueva Incidencia</a>
</div>

<div class="col-12 top-30">
  @include('flash::message')
</div>

<div class="col mayuscula">
<table class="table top-30">
  <thead class="thead-inverse">
    <tr>
      <th>#</th>
      <th>Departamento</th>
      <th>Concepto</th>
      <th>Trabajador</th>
      <th>Estatus</th>
      <th>Dia Publicaci&oacute;n</th>
      <th style="text-align: center;">Opciones</th>
    </tr>
  </thead>
  <tbody>
      @foreach($incidents as $insident)
        <tr>
          <td>{{$insident->id}}</td>
          <td>{{$insident->deparment}}</td>
          <td>{{$insident->title}}</td>
          <td>{{$insident->worker}}</td>
          <td>{{$insident->status}}</td>
          <td>{{$insident->created_at->format('d/m/Y')}}</td>
          <td style="text-align: center;">
              <a href="{{ route ('incidents.show',$insident->id) }}"><i class="fas fa-eye"></i></a> 
          </td>
            </div>
          </td>  
        </tr>
      @endforeach
  </tbody>
</table>
</div>
    
@endsection