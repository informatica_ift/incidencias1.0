@extends('template.main3')


@section('title') 
  Crear Departamento
@endsection

@section('content')

{!! Form::open(['route' => ['departamentos.update',$setting->id],'method' => 'PUT', 'files' => false,'class' => 'col-12']) !!}
<div class="col-12 top-30">
  @include('flash::message')
</div> 
@if(count($errors) > 0)
<div class="alert alert-warning" role="alert">
    <ul>
    @foreach($errors->all() as $error)
        <li><strong>Warning!</strong>{!!$error!!}</li>
    @endforeach 
    </ul>   
</div>
@endif

  <div class="form-group">
    <h1>Crear Departamento</h1>
    {{Form::label('deparment', 'Departamento')}}
    {{Form::text('deparment', $setting->deparment, ['class' => 'form-control'])}}
    {{Form::label('email', 'Correo')}}
    {{Form::text('email', $setting->email, ['class' => 'form-control'])}}
    <label for="deparment">Prioridad:</label>
    <select class="custom-select custom-select-lg mb-3" name="priority">
        <option value="null">Seleccionar una opci&oacute;n...</option> 
        <option value="1">1</option> 
        <option value="2">2</option> 
        <option value="3">3</option> 
        <option value="4">4</option> 
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
        <option value="9">9</option>
        <option value="10">10</option>
    </select>
    {{Form::label('deparment', $setting->priority)}}
    <br/>
    {{Form::submit('Actualizar',['class' => 'btn btn-primary'])}}
    <a href="{{ url ('dashboardsistem/back') }}" class="btn btn-primary "><i class="fas fa-undo"></i>&nbsp;Volver</a> 
  {!! Form::close() !!}
  
@endsection