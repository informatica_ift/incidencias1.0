@extends('template.main')


@section('title') 
  Incidencia
@endsection

@section('content') 


<div class="col-12 top-30">
  @include('flash::message')
</div>

<div class="col mayuscula">
<table class="table top-30">
  <thead class="thead-inverse">
    <tr>
      <th>Departamento</th>
      <th>Concepto</th>
      <th>Trabajador</th>
      <th>Dia Publicaci&oacute;n</th>
      <th style="text-align: center;">Opciones</th>
    </tr>
  </thead>
  <tbody>
      @foreach($incidents as $insident)
        <tr>
          <td>{{}}</td>
          <td>{{}}</td>
          <td>{{}}</td>
          <td>{{}}</td>
          <td style="text-align: center;">
              <a href="{{ url ('solves/add',$insident->id ) }}"><i class="fas fa-eye"></i></a> 
          </td>
            </div>
          </td>  
        </tr>
      @endforeach
  </tbody>
</table>
</div>
    
@endsection