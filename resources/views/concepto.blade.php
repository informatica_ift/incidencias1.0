@extends('template.main3')


@section('title') 
  Usuarios
@endsection

@section('content') 


<div class="col-12 top-30">
  @include('flash::message')
</div>

<div class="col-12 bottom">
  <a href="{{ url ('conceptos/new') }}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"><i class="fas fa-plus"></i>&nbsp;Nuevo Concepto</a>
</div>

<div class="col mayuscula">
<table class="table top-30">
  <thead class="thead-inverse">
    <tr>
      <th>#</th>
      <th>Nombre</th>
      <th>Departamento</th>
      <th>Correo</th>
    </tr>
  </thead>
  <tbody>
      @foreach($conceptos as $concepto)
        <tr>
          <td>{{$concepto->id}}</td>
          <td>{{$concepto->nombre}}</td>
          <td>{{$concepto->deparment}}</td>
          <td style="text-align: center;">
              <a href="{{ route ('conceptos.show',$concepto->id) }}" class=""><i class="fas fa-eye"></i></a>
              {!! Form::open(['route' => ['conceptos.destroy',$concepto->id] ,'method' => 'DELETE', 'files' => false,'class' => 'col-12']) !!}
              <button type="submit" class="btn-trash" onclick="return confirm('Deseas borrar este departamento click ACEPTAR')"><i class="fas fa-trash"></i></button>
              {!! Form::close() !!}    

          </td>
            </div>
          </td>  
        </tr>
      @endforeach
  </tbody>
  
</table>
<a href="{{ url ('dashboardsistem/back') }}" class="btn btn-primary "><i class="fas fa-undo"></i>&nbsp;Volver</a>  
</div>
  
@endsection