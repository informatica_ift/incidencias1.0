
@extends('template.main2')


@section('title') 
   Bienvenido 
@endsection


@section('content') 
<div class="flex-container top col-12">
        <div class="flex-item">   
            @if(!empty($errors))
                @foreach ($errors->all() as $error)
                   <div>{{ $error }}</div>
               @endforeach
            @endif    
        </div>
    </div>
<div class="col-12 top-30">
  @include('flash::message')
</div>
<div class="flex-container2 top">
    <div class="flex-item2">
        
        {{ Form::open(['route' => 'login.store']) }}
                <input type='hidden' name="ip" id="my-ip">    
                {!! Form::submit('Reportar Incidencia', ['class' => 'btn btn-outline-warning']) !!}      
        {{ Form::close() }}
         
    </div>
</div>



@endsection
