@extends('template.main3')


@section('title') 
   Panel Admin WebMaster
@endsection

@section('content') 
<div class="card col-12 ">
  <div class="card-body">
    Departamento: {{$usuario->deparment}}<br/>
    Responsable: {{$usuario->name}}<br/>
    Correo contacto: {{$usuario->email}}<br/>
  </div>
</div>

<div class="card top espacio" style="width: 10rem; height:10rem">
<a href="{{ route ('usuarios.index') }}">
    <img class="card-img-top" src="{{ asset('images/user.png') }}" alt="reporte insidencia">
</a>  
  
</div>

<div class="card top espacio" style="width: 10rem; height:10rem">
<a href="{{ route ('departamentos.index') }}">
    <img class="card-img-top" src="{{ asset('images/deparment.jpg') }}" alt="reporte insidencia">
</a>  
  
</div>

<div class="card top espacio" style="width: 10rem; height:10rem">
<a href="{{ route ('conceptos.index') }}">
    <img class="card-img-top" src="{{ asset('images/concepto.jpg') }}" alt="reporte insidencia">
</a>  
  
</div>
@endsection