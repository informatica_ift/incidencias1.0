@extends('template.main3')


@section('title') 
  Usuarios
@endsection

@section('content') 


<div class="col-12 top-30">
  @include('flash::message')
</div>
<div class="col-12 bottom">
  <a href="{{ url ('departamentos/new') }}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"><i class="fas fa-user-plus"></i>&nbsp;Nuevo Departamento</a>
  </div>
<div class="col mayuscula">
<table class="table top-30">
  <thead class="thead-inverse">
    <tr>
      <th>#</th>
      <th>Departamento</th>
      <th>Correo</th>
      <th>Prioridad</th>
    </tr>
  </thead>
  <tbody>
      @foreach($settings as $setting)
        <tr>
          <td>{{$setting->id}}</td>
          <td>{{$setting->deparment}}</td>
          <td>{{$setting->email}}</td>
          <td>{{$setting->priority}}</td>
          <td style="text-align: center;">
              <a href="{{ route ('departamentos.show',$setting->id) }}" class=""><i class="fas fa-eye"></i></a>
              {!! Form::open(['route' => ['departamentos.destroy',$setting->id] ,'method' => 'DELETE', 'files' => false,'class' => 'col-12']) !!}
              <button type="submit" class="btn-trash" onclick="return confirm('Deseas borrar este departamento click ACEPTAR')"><i class="fas fa-trash"></i></button>
              {!! Form::close() !!}    

          </td>
            </div>
          </td>  
        </tr>
      @endforeach
  </tbody>
  
</table>
<a href="{{ url ('dashboardsistem/back') }}" class="btn btn-primary "><i class="fas fa-undo"></i>&nbsp;Volver</a>  
</div>
  
@endsection