@extends('template.main')


@section('title') 
  Respuesta
@endsection

@section('content')



<div class="form-group col-12">
    
    <label for="title">Departamento:</label>
    <input type="text" name="deparment" value="{{$answer->deparment}}" disabled class="form-control mayuscula">
    <label for="title">Concepto:</label>
    <input type="text" name="title" value="{{$answer->title}}" disabled class="form-control mayuscula">
    <label for="worker">Firma:</label>
    <input type="text" name="worker" value="{{$answer->worker}}" disabled class="form-control mayuscula">
    <label for="worker">Responsable:</label>
    <input type="text" name="responsable" value="{{$usuarior->name}}" disabled class="form-control mayuscula">
    <label for="created_at">Fecha:</label>
    <input type="text" name="created_at" value="{{$answer->created_at->format('d/m/Y')}}" disabled class="form-control ">
    <label for="descript">Observaci&oacute;n:</label>
    <textarea name="descript" rows="3" disabled class="form-control">{{$answer->descript}}</textarea>
    <label for="descript">Soluci&oacute;n:</label>
    <textarea name="solve" rows="3" class="form-control" disabled>{{$answer->solve}}</textarea>
    @isset($answer->photo)
      @if ($answer->photo <> "no-imagen.png")
        <label class="col-12">Imagen referencia</label>
        <img src="../images/incidents/{{$answer->photo}}" class="col-12">
      @endif
    @endisset
    <br/>
    @isset($answer->file)
      <a href="../images/pdf/{{$answer->file}}"target="_blank" class="vermanual">Ver Manual</a>
    @endisset
    <br/>
  
  <a href="{{ url ('answers/list', $answer->iduser) }}" class="btn btn-primary "><i class="fas fa-undo"></i>&nbsp;Volver</a>  
  
</div>
 
@endsection