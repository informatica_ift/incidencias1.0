@extends('template.main3')


@section('title') 
  Usuarios
@endsection

@section('content') 


<div class="col-12 top-30">
  @include('flash::message')
</div>
<div class="col-12 bottom">
<a href="{{ url ('usuario/new') }}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"><i class="fas fa-user-plus"></i>&nbsp;Nuevo Usuario</a>
</div>

<div class="col mayuscula">
<table class="table top-30">
  <thead class="thead-inverse">
    <tr>
      <th>#</th>
      <th>Nombre</th>
      <th>Departamento</th>
      <th>Correo</th>
    </tr>
  </thead>
  <tbody>
      @foreach($usuarios as $usuario)
        <tr>
          <td>{{$usuario->id}}</td>
          <td>{{$usuario->name}}</td>
          <td>{{$usuario->deparment}}</td>
          <td>{{$usuario->email}}</td>
          <td style="text-align: center;">
            <a href="{{ route ('usuarios.show',$usuario->id) }}" class=""><i class="fas fa-eye"></i></a>
            {!! Form::open(['route' => ['usuarios.destroy',$usuario->id] ,'method' => 'DELETE', 'files' => false,'class' => 'col-12']) !!}
            <button type="submit" class="btn-trash" onclick="return confirm('You sure you want to DELETE this user')"><i class="fas fa-trash"></i></button>
            {!! Form::close() !!}
          </td>
            </div>
          </td>  
        </tr>
      @endforeach
  </tbody>
  
</table>
<a href="{{ url ('dashboardsistem/back') }}" class="btn btn-primary "><i class="fas fa-undo"></i>&nbsp;Volver</a>  
</div>
  
@endsection