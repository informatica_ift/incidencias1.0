<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $faker = Faker::create();
        DB::table('users')->insert([
            'name' => 'Carlos',
            'email' => 'informatica@cero69.com',
            'deparment' => 'informatica',
            'password' => '',
            'ip'       => '80.24.37.78',
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTime($max = 'now'),
            'updated_at' => $faker->dateTime($max = 'now'),
        ]);
        DB::table('users')->insert([
            'name' => 'Ricardo',
            'email' => 'informatica3@cero69.com',
            'deparment' => 'informatica',
            'password' => '',
            'ip'       => null,
            'remember_token' => str_random(10),
            'created_at' => $faker->dateTime($max = 'now'),
            'updated_at' => $faker->dateTime($max = 'now'),
        ]);
    }
}
