<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
        DB::table('settings')->insert([
            'deparment' => 'informatica',
            'email'     => 'informatica@cero69.com',
            'priority' => '1',
            'created_at' => $faker->dateTime($max = 'now'),
            'updated_at' => $faker->dateTime($max = 'now'),
        ]);
    }
}
