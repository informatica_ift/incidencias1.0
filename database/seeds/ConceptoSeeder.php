<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ConceptoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        DB::table('concepto')->insert([
            'nombre' => 'tpv',
            'deparment' => 'informatica',
            'created_at' => $faker->dateTime($max = 'now'),
            'updated_at' => $faker->dateTime($max = 'now'),
        ]);
    }
}
