<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class IncidentTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        DB::table('incidents')->insert([
            'deparment' => 'informatica',
            'worker'     => 'ricardo',
            'title' => 'instalacion',
            'descript' => 'instalacion de herramienta',
            'photo' => 'no-image.png',
            'iduser' => '2',
            'created_at' => $faker->dateTime($max = 'now'),
            'updated_at' => $faker->dateTime($max = 'now'),
        ]);
      
    }
    
}
