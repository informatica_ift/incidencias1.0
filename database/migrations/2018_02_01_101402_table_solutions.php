<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableSolutions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solutions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idincident',10)->nullable();
            $table->string('iduser',10)->nullable();
            $table->string('deparment',95)->nullable();
            $table->string('descript',255)->nullable();
            $table->string('solve',255)->nullable();
            $table->string('worker',65)->nullable();
            $table->string('photo',255)->nullable();
            $table->timestamps('date_publi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solutions');
    }
}
