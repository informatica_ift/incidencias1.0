<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::resource('login', 'LoginController');

Route::post('login/user', 'LoginController@user');

Route::resource('dashboard', 'DashboardController');

Route::resource('incidents', 'InsidenciaController');

Route::get('incidents/add/{id}', 'InsidenciaController@add');

Route::get('incidents/list/{id}', 'InsidenciaController@list');

Route::resource('solves', 'SolucionController');

Route::get('solves/list/{id}', 'SolucionController@list');

Route::post('solves/add', 'SolucionController@add');

Route::resource('answers', 'AnswerController');

Route::get('answers/list/{id}', 'AnswerController@list');

Route::resource('data', 'DataController');

Route::resource('email', 'EmailController');

Route::get('departamentos/new', 'DepartamentoController@new');

Route::resource('departamentos', 'DepartamentoController');

Route::get('conceptos/new', 'ConceptoController@new');

Route::resource('conceptos', 'ConceptoController');

Route::get('usuario/new', 'UsuarioController@new');

Route::resource('usuarios', 'UsuarioController');

Route::get('dashboardsistem/back', 'DashboardsistemController@back');

Route::resource('dashboardsistem', 'DashboardsistemController');




Route::get('sendemail', function(){

    $data = array(
        'mensaje' =>'hola',
    );

    Mail::send('emails.mail',$data,function($ms){

        $ms->from('informatica@cero69.com', 'Incidencias Web');

        $ms->to('informatica3@cero69.com')->subject('Correo');
    });

    return "tu email se ha enviado";
});