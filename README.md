# README #

Programa ara el reporte de incidencias por departamento

### Version de programa ###

Version 1.0

### Instalar software ###

1-Clonar repositorio, url clone: git clone https://informatica_ift@bitbucket.org/informatica_ift/incidencias1.0.git

2-Correr composer update

3-Cambiar nombre .env-example a .env.

4-Ejecutar comando php artisan migrate.

Nota: el equipo debe tener instalado la libreria composer, git, consola mysql. Para ver el desarrollo en local debe ejecutar comando #php artisan serve#.
### Desarrollado por Departamento de informatica ###

